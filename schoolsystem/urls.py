from django.urls import path

from . import views

app_name = 'schoolsystem'

urlpatterns = [
    path('', views.subject, name='index'),
    path('subject/', views.subject, name='subject'),
    path('course/', views.course, name='course'),
    path('teacher/', views.teacher, name='teacher'),
    path('class/', views.class_, name='class'),
    path('student/', views.student, name='student'),

]
