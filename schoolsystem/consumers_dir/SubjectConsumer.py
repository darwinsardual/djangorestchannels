import json

from schoolsystem.models import Subject, Teacher, Course
from schoolsystem.serializers import SubjectSerializer, TeacherSerializer, CourseSerializer

from .BaseConsumer import BaseConsumer


class SubjectConsumer(BaseConsumer):

    room_group_name = 'subject'

    def subject_create(self, event):
        action = event['action']
        data = event['data']

        serializer = self.new_serializer(
            data={
                'name': data['name'],
                'code': data['code'],
                'year_level': data['year_level']
            },
            instance=None,
            serializer_class=SubjectSerializer
        )

        self.create_update(action, serializer)

    def subject_update(self, event):
        action = event['action']
        data = event['data']

        subject = Subject.objects.get(id=data['id'])
        serializer = self.new_serializer(
            data={
                'name': data['name'],
                'code': data['code'],
                'year_level': data['year_level']
            },
            instance=subject,
            serializer_class=SubjectSerializer
        )

        self.create_update(action, serializer)

    def subject_delete(self, event):
        action = event['action']
        data = event['data']

        subject = Subject.objects.get(id=data['id'])
        self.delete(action, subject)

    def subject_list(self, event):
        action = event['action']
        queryset = Subject.objects.all()
        serializer = self.new_serializer(
            serializer_class=SubjectSerializer,
            queryset=queryset,
            many=True
        )

        self.list(action, serializer)

    def subject_details(self, event):
        action = 'details'
        data = event['data']

        subject = Subject.objects.get(id=data['id'])
        subject_serializer = SubjectSerializer(subject)

        teacher_query = Teacher.objects.filter(subjects__id=data['id'])
        teacher_serializer = TeacherSerializer(teacher_query, many=True)

        course_query = Course.objects.filter(subjects__id=data['id'])
        course_serializer = CourseSerializer(course_query, many=True)

        self.send(
            text_data=json.dumps({
                'message': {
                    'action': 'details',
                    'data': {
                        'id': data['id'],
                        'subject': subject_serializer.data,
                        'teachers': teacher_serializer.data,
                        'courses': course_serializer.data
                    }
                }
            })
        )
