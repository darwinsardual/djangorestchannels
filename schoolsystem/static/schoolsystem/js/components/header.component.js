class HeaderComponent extends React.Component{

  constructor(props){
    super(props);

    let domain = 'http://localhost:8000/';
    this.urls = {
      subject: domain + 'schoolsystem/subject/',
      course: domain + 'schoolsystem/course/',
      class: domain + 'schoolsystem/class/',
      teacher: domain + 'schoolsystem/teacher/',
      student: domain + 'schoolsystem/student/',
    };
  }

  createNavigation(urls){

    let elementList = [];
    
  }

  render(){
    let c = React.createElement;

    return c('header', {className: 'mdl-layout__header'},
      c('div', {className: "mdl-layout__header-row"},
        c('span', {className:'mdl-layout-title', value: 'School System'}, 'School System'),
        c('div',{className: 'mdl-layout-spacer'}, null),
        c('nav', {className: 'mdl-navigation'},
          c('a', {className: 'mdl-navigation__link', href: this.urls.subject}, 'Subject'),
          c('a', {className: 'mdl-navigation__link', href: this.urls.course}, 'Course'),
          c('a', {className: 'mdl-navigation__link', href: this.urls.class}, 'Class'),
          c('a', {className: 'mdl-navigation__link', href: this.urls.teacher}, 'Teacher'),
          c('a', {className: 'mdl-navigation__link', href: this.urls.student}, 'Student'),
        )
      )
    );
  }
}
