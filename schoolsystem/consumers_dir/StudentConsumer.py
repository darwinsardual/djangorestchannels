import json

from schoolsystem.models import Student, Class, Teacher, Subject
from schoolsystem.serializers import StudentSerializer, ClassSerializer, TeacherSerializer, SubjectSerializer

from .BaseConsumer import BaseConsumer


class StudentConsumer(BaseConsumer):

    room_group_name = 'student'

    def student_create(self, event):
        action = event['action']
        data = event['data']

        serializer = self.new_serializer(
            data={
                'first_name': data['first_name'],
                'last_name': data['last_name'],
                'year_level': data['year_level'],
                'course': data['course_id'],
                'classes': data['classes_id']
            },
            instance=None,
            serializer_class=StudentSerializer
        )
        self.create_update(action, serializer)

    def student_update(self, event):
        action = event['action']
        data = event['data']

        student = Student.objects.get(id=data['id'])
        serializer = self.new_serializer(
            data={
                'first_name': data['first_name'],
                'last_name': data['last_name'],
                'year_level': data['year_level'],
                'course': data['course_id'],
                'classes': data['classes_id']
            },
            instance=student,
            serializer_class=StudentSerializer
        )
        self.create_update(action, serializer)

    def student_delete(self, event):
        action = event['action']
        data = event['data']

        student = Student.objects.get(id=data['id'])
        self.delete(action, student)

    def student_list(self, event):
        action = event['action']

        queryset = Student.objects.all()
        serializer = self.new_serializer(
            serializer_class=StudentSerializer,
            queryset=queryset,
            many=True
        )

        self.list(action, serializer)

    def student_details(self, event):
        action = 'details'
        data = event['data']

        student = Student.objects.get(id=data['id'])
        student_serializer = StudentSerializer(student)
        # Get classes
        class_queryset = student.classes.all()
        class_serializer = ClassSerializer(class_queryset, many=True)

        # Get required subjects
        subject_queryset = student.course.subjects.all()
        subject_serializer = SubjectSerializer(subject_queryset, many=True)

        # Get class teacher
        teacher_query = Teacher.objects.filter(id__in=student.classes.all().values_list('teacher'))
        teacher_serializer = TeacherSerializer(teacher_query, many=True)


        self.send(
            text_data=json.dumps({
                'message': {
                    'action': action,
                    'data': {
                        'id': data['id'],
                        'student': student_serializer.data,
                        'classes_enrolled': class_serializer.data,
                        'subjects_required': subject_serializer.data,
                        'teachers': teacher_serializer.data
                    }
                }
            })
        )
