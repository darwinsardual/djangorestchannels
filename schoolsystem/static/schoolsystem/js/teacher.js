'use strict';

(function(){
  var headerComponent = document.querySelector('#header-component');
  ReactDOM.render(
    React.createElement(HeaderComponent, {}, null), headerComponent
  );

  var isTableInit = false;

  var firstnameInput = document.querySelector('#teacher-firstname');
  var lastnameInput = document.querySelector('#teacher-lastname');
  var subjectCheckbox = document.querySelectorAll('.teacher-subject-checkbox');
  var createButton = document.querySelector('#teacher-oncreate');
  var deleteButton = document.querySelector('#teacher-ondelete');

  var listTable = document.querySelector('#teacher-list');
  var tbody = listTable.getElementsByTagName('tbody')[0];


  function appendToTable(teacher){

    var row = document.createElement('tr');
    row.id = 'teacher_' + teacher.id;

    var firstnameItem = document.createElement('td');
    firstnameItem.textContent = teacher.first_name;
    firstnameItem.classList.add('mdl-data-table__cell--non-numeric');

    var lastnameItem = document.createElement('td');
    lastnameItem.textContent = teacher.last_name;
    lastnameItem.classList.add('mdl-data-table__cell--non-numeric');

    row.onclick = function(event){
      
      socket.send(JSON.stringify({
        'message': {
          'action': 'details',
          'data': {
            'id': teacher.id
          }
        }
      }))
    };

    row.appendChild(firstnameItem);
    row.appendChild(lastnameItem);

    tbody.appendChild(row)
  }

  function initTable(data){

    for(var teacher of data){
      appendToTable(teacher);
    }
  }

  function getSelectedSubjects(){
    var options = subjectsSelect.options;
    var selectedOptions = [];

    for(var option of options){

      if(option.selected)
        selectedOptions.push(parseInt(option.value));
    }

    return selectedOptions;
  }

  function displayDetails(data){

    setEditFields(data.teacher);

    deleteButton.onclick = function(event){
      socket.send(JSON.stringify({
        'message': {
            'action': 'delete',
            'data': {
              'id': data.id
            }
        }

      }));
    };
  }

  function removeFromTable(id){
    var tr = document.querySelector('#teacher_' + id);
    tbody.removeChild(tr);
  }

  function actionEvents(action, id=null){
    return function(event){
      var firstname = firstnameInput.value;
      var lastname = lastnameInput.value;
      var subjectsId = utils.getMultipleSelectedElementValue(subjectCheckbox, true);

      socket.send(JSON.stringify({
        'message': {
          'action': action,
          'data': {
            'id': id,
            'first_name': firstname,
            'last_name': lastname,
            'subjects_id': subjectsId,
          }
        }
      }));
    };
  }

  function setEditFields(teacher){

    firstnameInput.value = teacher.first_name;
    lastnameInput.value = teacher.last_name;
    utils.addClassNameToListParent('is-dirty', [lastnameInput, firstnameInput]);

    utils.clearSelection(subjectCheckbox);
    utils.removeClassNameToListParent('is-checked', subjectCheckbox);

    var selected = utils.setMultipleSelectedElement(teacher.subjects, subjectCheckbox);
    utils.addClassNameToListParent('is-checked', selected);

    createButton.textContent = 'Update';
    createButton.onclick = actionEvents('update', teacher.id);
  }

  const socket = new WebSocket('ws://localhost:8000/ws/schoolsystem/teacher/');

  socket.onopen = function(event){
    console.log('Socket connection opened');
    getListOfTeachers();
  };

  socket.onmessage = function(event){
    var data = JSON.parse(event.data);

    if(data.message.action == 'list' && !isTableInit){
      initTable(data.message.data);
      isTableInit = true;
    }else if(data.message.action == 'create'){
      if(data.message.status == true)
        appendToTable(data.message.data);
      else
        console.log(data.message.message)
    }else if(data.message.action == 'details'){
      displayDetails(data.message.data);
    }else if(data.message.action == 'delete'){
      removeFromTable(data.message.data.id);
    }
  };

  socket.onclose = function(event){
    console.log('Socket connection closed');
  };

  createButton.onclick = actionEvents('create');

  function getListOfTeachers(){
    socket.send(JSON.stringify({
      'message': {
        'action': 'list',
        'data': {

        }
      }
    }))
  };
})();
