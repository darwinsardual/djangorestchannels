from django.db import models

# Create your models here.

YEAR_LEVEL = (
    (1, '1st'),
    (2, '2nd'),
    (3, '3rd'),
    (4, '4th'),
    (5, '5th'),
    (6, '6th'),
    (7, '7th'),
)



class Subject(models.Model):
    name = models.CharField(max_length=100)
    code = models.CharField(max_length=30)
    year_level = models.PositiveSmallIntegerField(choices=YEAR_LEVEL, null=True)

    course_optional = models.ForeignKey('schoolsystem.Course', on_delete=models.CASCADE, null=True)


class Course(models.Model):
    name = models.CharField(max_length=100)
    code = models.CharField(max_length=30)

    subjects = models.ManyToManyField(Subject)


class Person(models.Model):
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)

    class Meta:
        abstract = True

class Student(Person):
    year_level = models.PositiveSmallIntegerField(choices=YEAR_LEVEL)

    course = models.ForeignKey(Course, on_delete=models.CASCADE)
    classes = models.ManyToManyField('schoolsystem.Class')


class Teacher(Person):
    subjects = models.ManyToManyField(Subject, related_name='subjects_allowed')



class Class(models.Model):
    name = models.CharField(max_length=50)
    max_capacity = models.PositiveSmallIntegerField()

    subject = models.ForeignKey(Subject, on_delete=models.CASCADE)
    teacher = models.ForeignKey(Teacher, on_delete=models.CASCADE)
