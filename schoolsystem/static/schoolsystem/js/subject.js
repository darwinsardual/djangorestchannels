// get DOM elements
// for create
'use strict';

(function(){

  var headerComponent = document.querySelector('#header-component');
  ReactDOM.render(
    React.createElement(HeaderComponent, {}, null), headerComponent
  );

  var isTableInit = false;
  var nameInput = document.querySelector('#subject-name');
  var codeInput = document.querySelector('#subject-code');
  var yearLevelInput = document.querySelector('#subject-year-level-hidden');
  var createButton = document.querySelector('#subject-oncreate');
  var deleteButton = document.querySelector('#subject-ondelete');

  var teacherSubjectDiv = document.querySelector('#teacher-subject-container');
  var requiredByCourseDiv = document.querySelector('#required-by-course-container');

  var listTable = document.querySelector('#subject-list');
  var tbody = listTable.getElementsByTagName('tbody')[0];

  var nameEditInput = document.querySelector('#subject-name-edit');
  var codeEditInput = document.querySelector('#subject-code-edit');
  var yearLevelLi = document.querySelectorAll('#year-level-select-edit>li');
  var yearLevelEditInput = document.querySelector('#subject-year-level-edit-hidden');
  var yearLevelEditInput2 = document.querySelector('#subject-year-level-edit');
  var editButton = document.querySelector('#subject-onedit');

  function appendToTable(subject){

    var row = document.createElement('tr');
    row.id = 'subject_' + subject.id;

    var nameItem = document.createElement('td');
    nameItem.textContent = subject.name;
    nameItem.className = "mdl-data-table__cell--non-numeric";

    var codeItem = document.createElement('td');
    codeItem.textContent = subject.code;

    var yearLevelItem = document.createElement('td');
    yearLevelItem.textContent = subject.year_level;

    row.onclick = function(event){
      socket.send(JSON.stringify({
        'message': {
          'action': 'details',
          'data': {
            'id': subject.id
          }
        }
      }))
    };

    row.appendChild(nameItem);
    row.appendChild(codeItem);
    row.appendChild(yearLevelItem);

    tbody.appendChild(row)
  }

  function initTable(data){

    for(var subject of data){
      appendToTable(subject);
    }
  }

  function getListOfSubjects(){
    socket.send(JSON.stringify({
      'message': {
        'action': 'list',
        'data': {

        }
      }
    }))
  };

  function displayDetails(data){

    teacherSubjectDiv.innerHTML = '';
    for(var teacher of data.teachers){

      var div = document.createElement('div');
      div.textContent = teacher.first_name + ' ' + teacher.last_name;

      teacherSubjectDiv.appendChild(div);
    }

    requiredByCourseDiv.innerHTML = '';
    for(var course of data.courses){
      var div = document.createElement('div');
      div.textContent = course.name;

      requiredByCourseDiv.appendChild(div);
    }

    setEditFields(data.subject);

    deleteButton.onclick = function(event){

      socket.send(JSON.stringify({
        'message': {
          'action': 'delete',
          'data': {
            'id': data.id
          }
        }
      }));
    };
  }

  function setEditFields(subject){
    nameEditInput.parentNode.classList.add('is-dirty');
    nameEditInput.value = subject.name;
    // nameEditInput.focus();

    codeEditInput.parentNode.classList.add('is-dirty');
    codeEditInput.value = subject.code;
    // codeEditInput.focus();

    for(var li of yearLevelLi){
      if(li.dataset.val == subject.year_level){
        yearLevelEditInput.value = subject.year_level;
        break;
      }
    }

    editButton.onclick = function(event){
      var subjectName = nameEditInput.value;
      var subjectCode = codeEditInput.value;
      var subjectYearLevel = yearLevelEditInput.value;

      subjectYearLevel = subjectYearLevel == '' ? null : parseInt(subjectYearLevel);

      socket.send(JSON.stringify({
        'message': {
          'action': 'update',
          'data': {
            'id': subject.id,
            'name': subjectName,
            'code': subjectCode,
            'year_level': subjectYearLevel
          }
        }
      }));
    }
  }

  function deleteFromTable(id){
    var tr = document.querySelector('#subject_' + id);
    tbody.removeChild(tr);
  }

  const socket = new WebSocket('ws://localhost:8000/ws/schoolsystem/subject/');

  socket.onopen = function(event){
    console.log('Socket connection opened');
    getListOfSubjects();
  };

  socket.onmessage = function(event){
    var data = JSON.parse(event.data);

    if(data.message.action == 'list' && !isTableInit){
      initTable(data.message.data);
      isTableInit = true;
    }else if(data.message.action == 'create'){
      if(data.message.status == true){
        console.log(data.message.message);
        appendToTable(data.message.data);
      }else{
        console.log(data.message.message);
      }
    }else if(data.message.action == 'details'){
      console.log(data.message.data);
      displayDetails(data.message.data);
    }else if(data.message.action == 'delete'){
      console.log(data.message.data);
      deleteFromTable(data.message.data.id);
    }
  };

  socket.onclose = function(event){
    console.log('Socket connection closed');
  };

  createButton.onclick = function(event){
    var subjectName = nameInput.value;
    var subjectCode = codeInput.value;
    var subjectYearLevel = yearLevelInput.value;

    subjectYearLevel = subjectYearLevel == '' ? null : parseInt(subjectYearLevel);


    socket.send(JSON.stringify({
      'message': {
        'action': 'create',
        'data': {
          'name': subjectName,
          'code': subjectCode,
          'year_level': subjectYearLevel
        }
      }
    }));
  };


})();
