import json

from django.utils.safestring import mark_safe
from django.shortcuts import render
from rest_framework.renderers import JSONRenderer

from .models import Subject, Teacher, Subject, Course, Class, YEAR_LEVEL
from .serializers import TeacherSerializer, SubjectSerializer, CourseSerializer, ClassSerializer

# Create your views here.


def subject(request):

    return render(request, 'schoolsystem/subject.html', {
        'context': {
            'YEAR_LEVEL': YEAR_LEVEL,
        }
    })


def course(request):

    subjects = Subject.objects.all()

    return render(request, 'schoolsystem/course.html', {
        'context': {
            'subjects': subjects
        }
    })


def teacher(request):

    subjects = Subject.objects.all()

    return render(request, 'schoolsystem/teacher.html', {
        'context': {
            'subjects': subjects,
        }
    })


def class_(request):

    teachers = Teacher.objects.all()
    subjects = Subject.objects.all()

    return render(request, 'schoolsystem/class.html', {
        'context': {
            'teachers': teachers,
            'subjects': subjects
        }
    })


def student(request):

    courses = Course.objects.all()
    classes = Class.objects.all()
    course_serializer = CourseSerializer(courses, many=True)
    class_serializer = ClassSerializer(classes, many=True)

    return render(request, 'schoolsystem/student.html', {
        'context': {
            'YEAR_LEVEL': YEAR_LEVEL,
            'courses': courses,
            'classes': classes,
        }
    })
