'use strict';

(function(){
  var headerComponent = document.querySelector('#header-component');
  ReactDOM.render(
    React.createElement(HeaderComponent, {}, null), headerComponent
  );

  var isTableInit = false;

  var firstnameInput = document.querySelector('#student-firstname');
  var lastnameInput = document.querySelector('#student-lastname');
  var yearLevelInput = document.querySelector('#student-year-level-hidden');
  var courseInput = document.querySelector('#student-course-hidden');
  var classCheckbox = document.querySelectorAll('.student-class-checkbox');
  var createButton = document.querySelector('#student-oncreate');

  var deleteButton = document.querySelector('#student-ondelete');

  var classEnrolledDiv = document.querySelector('#class-enrolled-container');
  var subjectRequiredDiv = document.querySelector('#subject-required-container');
  var teacherStudentDiv = document.querySelector('#teacher-student-container');

  var yearLevelLi = document.querySelectorAll('#year-level-select>li');
  var courseLi = document.querySelectorAll('#course-select>li');

  var listTable = document.querySelector('#student-list');
  var tbody = listTable.getElementsByTagName('tbody')[0];

  function getSelectedClasses(){
    var options = classCheckbox.options;
    var selectedOptions = [];

    for(var option of options){

      if(option.selected)
        selectedOptions.push(parseInt(option.value));
    }

    return selectedOptions;
  }

  function getStudentDetails(id){
    socket.send(JSON.stringify({
      'message': {
        'action': 'details',
        'data': {
          'id': id
        }
      }
    }));
  }

  function appendToTable(student){
    var row = document.createElement('tr');
    row.id = 'student_' + student.id;

    var firstnameItem = document.createElement('td');
    firstnameItem.textContent = student.first_name;
    firstnameItem.classList.add('mdl-data-table__cell--non-numeric');

    var lastnameItem = document.createElement('td');
    lastnameItem.textContent = student.last_name;
    lastnameItem.classList.add('mdl-data-table__cell--non-numeric');

    var yearLevelItem = document.createElement('td');
    yearLevelItem.textContent = student.year_level;

    row.onclick = function(event){


      socket.send(JSON.stringify({
        'message': {
          'action': 'details',
          'data': {
            'id': student.id
          }
        }
      }));
    };

    row.appendChild(firstnameItem);
    row.appendChild(lastnameItem);
    row.appendChild(yearLevelItem);

    tbody.appendChild(row)
  }

  function initTable(students){
    for(var student of students){
      appendToTable(student);
    }
  }

  function getListOfStudents(){
    socket.send(JSON.stringify({
      'message': {
        'action': 'list',
        'data': {

        }
      }
    }));
  }

  function displayDetails(data){

    classEnrolledDiv.innerHTML = '';

    for(var class_ of data.classes_enrolled){
      var div = document.createElement('div');
      div.textContent = class_.name;

      classEnrolledDiv.appendChild(div);
    }

    subjectRequiredDiv.innerHTML = '';

    for(var subject of data.subjects_required){
      var div = document.createElement('div');
      div.textContent = subject.name;

      subjectRequiredDiv.appendChild(div);
    }

    teacherStudentDiv.innerHTML = '';

    for(var teacher of data.teachers){
      var div = document.createElement('div');
      div.textContent = teacher.first_name + ' ' + teacher.last_name;

      teacherStudentDiv.appendChild(div);
    }

    setEditFields(data.student);

    deleteButton.onclick = function(event){
      socket.send(JSON.stringify({
        'message': {
          'action': 'delete',
          'data': {
            'id': data.id
          }
        }
      }));
    };
  }

  function deleteFromTable(id){
    var tr = document.querySelector('#student_' + id);
    tbody.removeChild(tr);
  }

  function actionEvents(action, id=null){
    yearLevelInput.value = 3;
    return function(event){

      var firstname = firstnameInput.value;
      var lastname = lastnameInput.value;
      var yearLevel = yearLevelInput.value;
      var course = courseInput.value;
      var classes = utils.getMultipleSelectedElementValue(classCheckbox, true);

      yearLevel = yearLevel == ''? null: parseInt(yearLevel);
      course = course == ''? null: parseInt(course);

      socket.send(JSON.stringify({
        'message': {
          'action': action,
          'data': {
            'id': id,
            'first_name': firstname,
            'last_name': lastname,
            'year_level': yearLevel,
            'course_id': course,
            'classes_id': classes
          }
        }
      }));

    };
  }

  function setEditFields(student){

    utils.clearSelection(classCheckbox);
    utils.removeClassNameToListParent('is-checked', classCheckbox);

    firstnameInput.value = student.first_name;
    lastnameInput.value = student.last_name;

    utils.addClassNameToListParent('is-dirty', [firstnameInput, lastnameInput]);

    var selected = utils.setMultipleSelectedElement(student.classes, classCheckbox);
    utils.addClassNameToListParent('is-checked', selected);

    for(var li of yearLevelLi){
      if(li.dataset.val == student.year_level){
        yearLevelInput.value = student.year_level;
        break;
      }
    }

    for(var li of courseLi){
      if(li.dataset.val == student.course){
        courseInput.value = student.course;
        break;
      }
    }

    createButton.textContent = 'Update';
    createButton.onclick = actionEvents('update', student.id);

  }

  const socket = new WebSocket('ws://localhost:8000/ws/schoolsystem/student/');

  socket.onopen = function(event){
    console.log('Socket connection opened');
    getListOfStudents();
  };

  socket.onmessage = function(event){
    var data = JSON.parse(event.data);

    if(data.message.action == 'list' && !isTableInit){
      initTable(data.message.data);
      isTableInit = true;
    }else if(data.message.action == 'create'){
      if(data.message.status == true){
        console.log(data.message.message);
        appendToTable(data.message.data);
      }else{
        console.log(data.message.message);
      }
    }else if(data.message.action == 'details'){

      displayDetails(data.message.data);
    }else if(data.message.action == 'delete'){
      console.log(data.message.data);
      deleteFromTable(data.message.data.id);
    }
  };

  socket.onclose = function(event){
    console.log('Socket connection closed');
  };

  createButton.onclick = actionEvents('create');
})();
