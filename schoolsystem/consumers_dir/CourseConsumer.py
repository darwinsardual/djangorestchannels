import json

from django.db.models import Count

from schoolsystem.models import Course, Teacher
from schoolsystem.serializers import CourseSerializer

from .BaseConsumer import BaseConsumer


class CourseConsumer(BaseConsumer):

    room_group_name = 'course'

    def course_create(self, event):
        action = event['action']
        data = event['data']

        serializer = self.new_serializer(
            data={
                'name': data['name'],
                'code': data['code'],
                'subjects': data['subjects']
            },
            instance=None,
            serializer_class=CourseSerializer
        )
        self.create_update(action, serializer)

    def course_update(self, event):
        action = event['action']
        data = event['data']

        course = Course.objects.get(id=data['id'])
        serializer = self.new_serializer(
            data={
                'name': data['name'],
                'code': data['code'],
                'subjects': data['subjects']
            },
            instance=course,
            serializer_class=CourseSerializer
        )

        self.create_update(action, serializer)

    def course_delete(self, event):
        action = event['action']
        data = event['data']

        course = Course.objects.get(id=data['id'])
        self.delete(action, course)

    def course_list(self, event):
        action = event['action']

        queryset = Course.objects.annotate(Count('student', distinct=True), Count('subjects__class__teacher', distinct=True))
        serializer = self.new_serializer(
            serializer_class=CourseSerializer,
            queryset=queryset,
            many=True
        )

        self.list(action, serializer)

    def course_details(self, event):
        action = 'details'
        data = event['data']

        course = Course.objects.get(id=data['id'])
        course_serializer = CourseSerializer(course);
        student_count = course.student_set.all().count()

        course_subjects = course.subjects.all()
        teacher_count = Teacher.objects.filter(subjects__in=course_subjects).count()

        self.send(
            text_data=json.dumps({
                'message': {
                    'action': action,
                    'data': {
                        'id': data['id'],
                        'course': course_serializer.data,
                        'student_count': student_count,
                        'teacher_count': teacher_count
                    }
                }
            })
        )
