var utils = {

  setMultipleSelectedElement: function(values, list /*nodelist*/ ){

    var selected = [];

    for(var element of list){

      if(values.includes(parseInt(element.value))){
        if(element.checked) element.checked = true;
        else element.selected = true;

        selected.push(element);
      }


    }

    return selected;
  },

  setSelectedElement: function(value, list){

    var selected;

      for(var element of list){
        if(value == parseInt(element.value)){
          if(element.checked) element.checked = true;
          else element.selected = true;

          selected = element
          break;
        }

      }
      console.log(value);
      console.log(selected.value);
      return selected;
  },

  clearSelection: function(list){
    for(var element of list){
      if(element.checked) element.checked = false;
      else element.selected = false;

    }

  },

  getSelectedElement: function(list){
    for(var element of list){
      if(element.selected || element.checked)
        return element;
    }

    return null;
  },

  getSelectedElementValue: function(list, isInt=false){
    var element = this.getSelectedElement(list);
    if(isInt)
      return parseInt(element.value);
    else
      return element.value;
  },

  getMultipleSelectedElement: function(list){
    var selected = [];

    for(var element of list){

      if(element.selected || element.checked)
        selected.push(element);
    }

    return selected;
  },

  getMultipleSelectedElementValue: function(list, isInt=false){
    var elementList = this.getMultipleSelectedElement(list);
    var valueList = [];

    if(isInt){
      for(var element of elementList){
        valueList.push(parseInt(element.value));
      }
    }else{
      for(var element of elementList){
        valueList.push(element.value);
      }
    }
    return valueList;
  },

  findSelectedElementById: function(id, list){
    for(var element of list){
      if(parseInt(element.id) == parseInt(id))
        return element;
    }
    return null;
  },

  addClassNameToListParent: function(className, list){
    for(var element of list){
      element.parentNode.classList.add(className);
    }
  },
  removeClassNameToListParent: function(className, list){
    for(var element of list){
      element.parentNode.classList.remove(className);
    }
  }
}
