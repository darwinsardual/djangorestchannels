from datetime import datetime
from rest_framework import serializers
from rest_framework.renderers import JSONRenderer

from .models import Subject, Course, Teacher, Class, Student


class CourseSerializer(serializers.ModelSerializer):
    student__count = serializers.IntegerField(required=False)
    subjects__class__teacher__count = serializers.IntegerField(required=False)

    class Meta:
        model = Course
        fields = ('id', 'name', 'code', 'subjects', 'student__count', 'subjects__class__teacher__count')

    def create(self, validated_data):
        subjects_id = validated_data.get('subjects')

        course = Course.objects.create(
            name=validated_data.get('name'),
            code=validated_data.get('code'),
        )

        course.subjects.set(subjects_id)

        return course

    def update(self, instance, validated_data):

        subjects_id = validated_data.get('subjects')

        instance.name = validated_data.get('name')
        instance.code = validated_data.get('code')
        instance.subjects.clear()

        instance.subjects.set(subjects_id)

        instance.save()

        return instance


class SubjectSerializer(serializers.ModelSerializer):

    class Meta:
        model = Subject
        fields = ('id', 'name', 'code', 'year_level')

    def create(self, validated_data):

        #course = Course.objects.get(pk=validated_data['course_id'])

        subject = Subject.objects.create(
            #course_optional=course,
            name=validated_data.get('name'),
            code=validated_data.get('code'),
            year_level=validated_data.get('year_level')
        )

        return subject

    def update(self, instance, validated_data):

        instance.name = validated_data.get('name')
        instance.code = validated_data.get('code')
        instance.year_level = validated_data.get('year_level')

        instance.save()

        return instance


class TeacherSerializer(serializers.ModelSerializer):
    # subject = SubjectSerializer()


    class Meta:
        model = Teacher
        fields = ('id', 'first_name', 'last_name', 'subjects')

    def create(self, validated_data):
        subjects_id = validated_data.get('subjects')

        teacher = Teacher.objects.create(
            first_name=validated_data.get('first_name'),
            last_name=validated_data.get('last_name')
        )

        teacher.subjects.set(subjects_id)

        return teacher

    def update(self, instance, validated_data):

        subjects_id = validated_data.get('subjects')

        instance.first_name = validated_data.get('first_name')
        instance.last_name = validated_data.get('last_name')
        instance.subjects.clear()

        instance.subjects.set(subjects_id)
        instance.save()

        return instance


class ClassSerializer(serializers.ModelSerializer):
    student__count = serializers.IntegerField(required=False)

    class Meta:
        model = Class
        fields = ('id', 'name', 'max_capacity', 'subject', 'teacher', 'student__count')

    def create(self, validated_data):

        subject = validated_data.get('subject')
        teacher = validated_data.get('teacher')

        class_ = Class.objects.create(
            name=validated_data.get('name'),
            max_capacity=validated_data.get('max_capacity'),
            subject=subject,
            teacher=teacher,
        )

        return class_

    def update(self, instance, validated_data):

        subject = validated_data.get('subject')
        teacher = validated_data.get('teacher')

        instance.name = validated_data.get('name')
        instance.max_capacity = validated_data.get('max_capacity')
        instance.subject = subject
        instance.teacher = teacher

        instance.save()

        return instance


    def validate(self, data):
        if not data['teacher'].subjects.filter(id=data['subject'].id).exists():
            raise serializers.ValidationError("Teacher can't teach this subject")

        return data



class StudentSerializer(serializers.ModelSerializer):

    class Meta:
        model = Student
        fields = ('id', 'first_name', 'last_name', 'year_level', 'course', 'classes')

    def create(self, validated_data):
        course = validated_data.get('course')
        classes_id = validated_data.get('classes')

        student = Student.objects.create(
            first_name=validated_data.get('first_name'),
            last_name=validated_data.get('last_name'),
            year_level=validated_data.get('year_level'),
            course=course
        )
        student.classes.set(classes_id)
        student.save()

        return student

    def update(self, instance, validated_data):
        course = validated_data.get('course')
        classes_id = validated_data.get('classes')

        instance.first_name = validated_data.get('first_name')
        instance.last_name = validated_data.get('last_name')
        instance.year_level = validated_data.get('year_level')
        instance.course = validated_data.get('course')

        instance.classes.clear()
        instance.classes.set(classes_id)
        instance.save()

        return instance

    def validate(self, data):

        for class_ in data['classes']:
            if class_.student_set.all().count() >= class_.max_capacity:
                raise serializers.ValidationError("Class already full")

        return data
