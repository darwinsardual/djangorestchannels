from django.apps import AppConfig


class SchoolsystemConfig(AppConfig):
    name = 'schoolsystem'
