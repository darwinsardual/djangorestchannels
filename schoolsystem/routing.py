from django.urls import path

from . import consumers

websocket_urlpatterns = [
    path('ws/schoolsystem/subject/', consumers.SubjectConsumer),
    path('ws/schoolsystem/course/', consumers.CourseConsumer),
    path('ws/schoolsystem/teacher/', consumers.TeacherConsumer),
    path('ws/schoolsystem/class/', consumers.ClassConsumer),
    path('ws/schoolsystem/student/', consumers.StudentConsumer),
]
