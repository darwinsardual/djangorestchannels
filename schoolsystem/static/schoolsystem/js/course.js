'use strict';

/* dependent on utils.js */

(function(){

  var headerComponent = document.querySelector('#header-component');
  ReactDOM.render(
    React.createElement(HeaderComponent, {}, null), headerComponent
  );

  var isTableInit = false;
  var nameInput = document.querySelector('#course-name');
  var codeInput = document.querySelector('#course-code');
  var subjectCheckbox = document.querySelectorAll('.course-subject-checkbox');
  var createButton = document.querySelector('#course-oncreate');
  var deleteButton = document.querySelector('#course-ondelete');

  var numStudentDiv = document.querySelector('#num-student-container');
  var numTeacherDiv = document.querySelector('#num-teacher-container');

  var listTable = document.querySelector('#course-list');
  var tbody = listTable.getElementsByTagName('tbody')[0];

  function getListOfCourses(){
    socket.send(JSON.stringify({
      'message': {
        'action': 'list',
        'data': {

        }
      }
    }));
  }

  function appendToTable(course){

    var row = document.createElement('tr');
    row.id = 'course_' + course.id;

    var nameItem = document.createElement('td');
    nameItem.textContent = course.name;
    nameItem.classList.add('mdl-data-table__cell--non-numeric');

    var codeItem = document.createElement('td');
    codeItem.textContent = course.code;
    codeItem.classList.add('mdl-data-table__cell--non-numeric');

    row.onclick = function(event){
      socket.send(JSON.stringify({
        'message': {
          'action': 'details',
          'data': {
            'id': course.id
          }
        }
      }));
    };

    row.appendChild(nameItem);
    row.appendChild(codeItem);

    tbody.appendChild(row)
  }

  function initTable(data){

    for(var course of data){
      appendToTable(course);
    }
  }

  function displayDetails(data){

    numStudentDiv.innerHTML = '';
    numStudentDiv.textContent = data.student_count;

    numTeacherDiv.innerHTML = '';
    numTeacherDiv.textContent = data.teacher_count;

    setEditFields(data.course);

    deleteButton.onclick = function(event){
      socket.send(JSON.stringify({
        'message': {
          'action': 'delete',
          'data': {
            'id': data.id
          }
        }
      }));
    };
  }

  function deleteFromTable(id){
    var tr = document.querySelector('#course_' + id);
    tbody.removeChild(tr);
  }

  function setEditFields(course){
    nameInput.value = course.name;
    nameInput.parentNode.classList.add('is-dirty');
    codeInput.value = course.code;
    codeInput.parentNode.classList.add('is-dirty');

    utils.clearSelection(subjectCheckbox);
    utils.removeClassNameToListParent('is-checked', subjectCheckbox);

    var selected = utils.setMultipleSelectedElement(course.subjects, subjectCheckbox);
    utils.addClassNameToListParent('is-checked', selected);

    createButton.textContent = 'Update';
    createButton.onclick = actionEvents('update', course.id);
  }

  function actionEvents(action, id=null){
    // only for create and update

    return function(event){
      var name = nameInput.value;
      var code = codeInput.value;
      var subjects = utils.getMultipleSelectedElementValue(subjectCheckbox, true);

      socket.send(JSON.stringify({
        'message': {
          'action': action,
          'data': {
            'id': id,
            'name': name,
            'code': code,
            'subjects': subjects
          }
        }
      }));

    };
  }

  const socket = new WebSocket('ws://localhost:8000/ws/schoolsystem/course/');

  socket.onopen = function(event){
    console.log('Socket connection opened');
    getListOfCourses();
  };

  socket.onmessage = function(event){
    var data = JSON.parse(event.data);

    if(data.message.action == 'list' && !isTableInit){
      initTable(data.message.data);
      isTableInit = true;
    }else if(data.message.action == 'create'){
      if(data.message.status == true)
        appendToTable(data.message.data);
      else
        console.log(data.message.message);
    }else if(data.message.action == 'details'){
      console.log(data.message.data);
      displayDetails(data.message.data);
    }else if(data.message.action == 'delete'){
      console.log(data.message.data);
      deleteFromTable(data.message.data.id);

    }
  };

  socket.onclose = function(event){
    console.log('Socket connection closed');
  };

  createButton.onclick = actionEvents('create');


})();
