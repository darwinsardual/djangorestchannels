import json

from django.db.models import Count

from schoolsystem.models import Class, Student
from schoolsystem.serializers import ClassSerializer, StudentSerializer

from .BaseConsumer import BaseConsumer


class ClassConsumer(BaseConsumer):

    room_group_name = 'class'

    def class_create(self, event):
        action = event['action']
        data = event['data']

        serializer = self.new_serializer(
            data={
                'name': data['name'],
                'max_capacity': data['max_capacity'],
                'teacher': data['teacher_id'],
                'subject': data['subject_id'],
            },
            instance=None,
            serializer_class=ClassSerializer
        )
        self.create_update(action, serializer)

    def class_update(self, event):
        action = event['action']
        data = event['data']

        class_ = Class.objects.get(id=data['id'])
        serializer = self.new_serializer(
            data={
                'name': data['name'],
                'max_capacity': data['max_capacity'],
                'subject': data['subject_id'],
                'teacher': data['teacher_id']
            },
            instance=class_,
            serializer_class=ClassSerializer
        )
        self.create_update(action, serializer)

    def class_delete(self, event):
        action = event['action']
        data = event['data']

        class_ = Class.objects.get(id=data['id'])
        self.delete(action, class_)

    def class_list(self, event):
        action = event['action']

        queryset = Class.objects.annotate(Count('student'))
        serializer = self.new_serializer(
            serializer_class=ClassSerializer,
            queryset=queryset,
            many=True,
        )
        self.list(action, serializer)

    def class_add_student(self, event):
        action = event['action']
        data = event['data']

        student_ids = data['student_ids']
        class_id = data['class_id']

        class_ = Class.objects.get(id=class_id)
        # class_.student_set.add(student_ids)
        for student in class_.student_set.all():
            student_serializer = StudentSerializer(
                instance=student,
                data={
                    'classes': [class_id]
                },
                partial=True
            )

            if student_serializer.is_valid():
                student_serializer.save()
            else:
                print(student_serializer.errors)


        self.send(
            text_data=json.dumps({
                'message': {
                    'action': action,
                    'data': {
                        'ids': student_ids
                    }
                }
            })
        )

    def class_remove_student(self, event):
        action = event['action']
        data = event['data']

        student_ids = data['student_ids'] # int array of ids
        class_id = data['class_id']

        class_ = Class.objects.get(id=class_id)
        class_.student_set.remove(student_ids)

        self.send(
            text_data=json.dumps({
                'message': {
                    'action': action,
                    'data': {
                        'ids': student_ids
                    }
                }
            })
        )

    def class_details(self, event):
        action = 'details'
        data = event['data']

        class_ = Class.objects.get(id=data['id'])
        class_serializer = ClassSerializer(class_)

        student_queryset = Student.objects.filter(classes__id=data['id'])
        student_serializer = StudentSerializer(student_queryset, many=True)

        self.send(
            text_data=json.dumps({
                'message': {
                    'action': action,
                    'data': {
                        'id': data['id'],
                        'class': class_serializer.data,
                        'students': student_serializer.data,
                    }
                }
            })
        )
