import json

from asgiref.sync import async_to_sync
from channels.generic.websocket import WebsocketConsumer


class BaseConsumer(WebsocketConsumer):

    # self.room_group_name must be defined on the child class
    room_group_name = ''

    def connect(self):
        async_to_sync(self.channel_layer.group_add)(
            self.room_group_name,
            self.channel_name,
        )
        self.accept()

    def disconnect(self, close_code):
        async_to_sync(self.channel_layer.group_discard)(
            self.room_group_name,
            self.channel_name,
        )

    def receive(self, text_data):
        converted_text_data = json.loads(text_data)
        message = converted_text_data['message']
        type = '%s_%s' % (self.room_group_name, message['action'])
        data = message['data']

        async_to_sync(self.channel_layer.group_send)(
            self.room_group_name,
            {
                'type': type,
                'action': message['action'],
                'data': data
            }
        )

    def create_update(self, action, serializer):

        status = None
        instance = None
        message = ''
        data = {}

        if serializer.is_valid():
            instance = serializer.save()
            status = True
            message = 'Transaction succesful'
            data = serializer.data

        else:
            status = False
            print(serializer.errors)
            message = serializer.errors

        self.send(
            text_data=json.dumps({
                'message': {
                    'action': action,
                    'status': status,
                    'message': message,
                    'data': data
                }
            })
        )


    def delete(self, action, instance, primary_key=None):

        id = primary_key or instance.id
        instance.delete()
        self.send(
            text_data=json.dumps({
                'message': {
                    'action': action,
                    'data': {
                        'id': id
                    }
                }
            })
        )

    def list(self, action, serializer):
        self.send(
            text_data=json.dumps({
                'message': {
                    'action': action,
                    'data': serializer.data
                }
            })
        )

    def new_serializer(self, data=None, instance=None, serializer_class=None,
        queryset=None, many=False):

        if instance is not None and data is not None:
            serializer = serializer_class(
                instance=instance,
                data=data
            )
        elif data is not None:
            serializer = serializer_class(
                data=data
            )
        else:
            serializer = serializer_class(
                queryset,
                many=many
            )

        return serializer
