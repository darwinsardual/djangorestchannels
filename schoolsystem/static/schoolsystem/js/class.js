'use strict';

(function(){
  var headerComponent = document.querySelector('#header-component');
  ReactDOM.render(
    React.createElement(HeaderComponent, {}, null), headerComponent
  );

  var isTableInit = false;

  var nameInput = document.querySelector('#class-name');
  var maxCapacityInput = document.querySelector('#class-max-capacity');
  var teacherRadio = document.querySelectorAll('.class-teacher-radio');
  var subjectRadio = document.querySelectorAll('.class-subject-radio');

  var studentClassDiv = document.querySelector('#student-class-container');
  var numStudentDiv = document.querySelector('#num-student-container');

  var classListTable = document.querySelector('#class-list');
  var tbody = document.getElementsByTagName('tbody')[0];

  var createButton = document.querySelector('#class-oncreate');
  var deleteButton = document.querySelector('#class-ondelete');

  function initTeacherSelect(teachers){

    for(var teacher of teachers){

      var option = document.createElement('option');
      option.textContent = teacher.first_name + ' ' + teacher.last_name;
      option.value = teacher.id;

      classTeacherSelect.appendChild(option);
    }
  }

  function getClassDetails(id){
    socket.send(JSON.stringify({
      'message': {
        'action': 'details',
        'data': {
          'id': id
        }
      }
    }));
  }

  function appendToTable(class_){
    var row = document.createElement('tr');
    row.id = 'class_' + class_.id;

    var nameItem = document.createElement('td');
    nameItem.textContent = class_.name;
    nameItem.classList.add('mdl-data-table__cell--non-numeric');

    var maxCapacityItem = document.createElement('td');
    maxCapacityItem.textContent = class_.max_capacity;
    //maxCapacity.classList.add('mdl-data-table__cell--numeric');

    row.onclick = function(event){

      socket.send(JSON.stringify({
        'message': {
          'action': 'details',
          'data': {
            'id': class_.id,
          }
        }
      }));
    };

    row.appendChild(nameItem);
    row.appendChild(maxCapacityItem);

    tbody.appendChild(row)
  }

  function initTable(classes){
    for(var class_ of classes){
      appendToTable(class_)
    }
  }

  function getListOfClasses(){
    socket.send(JSON.stringify({
      'message': {
        'action': 'list',
        'data': {

        }
      }
    }));
  }

  function actionEvents(action, id=null){
    return function(event){
      var name = nameInput.value;
      var maxCapacity = parseInt(maxCapacityInput.value);

      var teacherId = utils.getSelectedElementValue(teacherRadio, true);
      var subjectId = utils.getSelectedElementValue(subjectRadio, true);

      socket.send(JSON.stringify({
        'message': {
          'action': action,
          'data': {
            'id': id,
            'name': name,
            'max_capacity': maxCapacity,
            'teacher_id': teacherId,
            'subject_id': subjectId
          }
        }
      }));
    };
  }

  function displayDetails(data){

    studentClassDiv.innerHTML = '';

    for(var student of data.students){
      var div = document.createElement('div');
      div.textContent = student.first_name + ' ' + student.last_name;

      studentClassDiv.appendChild(div);
    }

    numStudentDiv.innerHTML = '';
    numStudentDiv.textContent = '' + data.students.length;

    setEditFields(data.class);

    deleteButton.onclick = function(event){
      socket.send(JSON.stringify({
        'message': {
          'action': 'delete',
          'data': {
            'id': data.id
          }
        }
      }))
    };
  }

  function deleteFromTable(id){
    var tr = document.querySelector('#class_' + id);
    tbody.removeChild(tr);
  }

  function setEditFields(class_){

    utils.clearSelection(teacherRadio);
    utils.removeClassNameToListParent('is-checked', teacherRadio);

    utils.clearSelection(subjectRadio);
    utils.removeClassNameToListParent('is-checked', subjectRadio);

    nameInput.value = class_.name;
    maxCapacityInput.value = class_.max_capacity;
    utils.addClassNameToListParent('is-dirty', [nameInput, maxCapacityInput]);

    var selectedTeacher = utils.setSelectedElement(class_.teacher, teacherRadio);
    var selectedSubject = utils.setSelectedElement(class_.subject, subjectRadio);

    utils.addClassNameToListParent('is-checked', [selectedTeacher, selectedSubject]);

    createButton.textContent = 'Update';
    createButton.onclick = actionEvents('update', class_.id);
  }

  const socket = new WebSocket('ws://localhost:8000/ws/schoolsystem/class/');

  socket.onopen = function(event){
    console.log('Socket connection opened');
    getListOfClasses();
  };

  socket.onmessage = function(event){
    var data = JSON.parse(event.data);

    if(data.message.action == 'list' && !isTableInit){

      initTable(data.message.data);
      isTableInit = true;
    }else if(data.message.action == 'create'){
      if(data.message.status == true){
        console.log(data.message.message);
        appendToTable(data.message.data);
      }
      else{
        console.log(data.message.message);
      }
    }else if(data.message.action == 'details'){
      displayDetails(data.message.data);

    }else if(data.message.action == 'delete'){

      deleteFromTable(data.message.data.id);
    }
  };

  socket.onclose = function(event){
    console.log('Socket connection closed');
  };

  createButton.onclick = actionEvents('create');

})();
