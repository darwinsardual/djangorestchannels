import json

from .models import Subject, Course, Teacher, Class, Student
from .serializers import SubjectSerializer, CourseSerializer, TeacherSerializer, ClassSerializer, StudentSerializer

from .consumers_dir.BaseConsumer import BaseConsumer
from .consumers_dir.SubjectConsumer import SubjectConsumer
from .consumers_dir.CourseConsumer import CourseConsumer
from .consumers_dir.TeacherConsumer import TeacherConsumer
from .consumers_dir.ClassConsumer import ClassConsumer
from .consumers_dir.StudentConsumer import StudentConsumer
