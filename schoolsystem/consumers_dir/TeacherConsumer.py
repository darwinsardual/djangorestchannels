import json

from schoolsystem.models import Teacher
from schoolsystem.serializers import TeacherSerializer

from .BaseConsumer import BaseConsumer


class TeacherConsumer(BaseConsumer):

    room_group_name = 'teacher'

    def teacher_create(self, event):
        action = event['action']
        data = event['data']

        serializer = self.new_serializer(
            data={
                'first_name': data['first_name'],
                'last_name': data['last_name'],
                'subjects': data['subjects_id']
            },
            instance=None,
            serializer_class=TeacherSerializer
        )
        self.create_update(action, serializer)

    def teacher_update(self, event):
        action = event['action']
        data = event['data']

        teacher = Teacher.objects.get(id=data['id'])
        serializer = self.new_serializer(
            data={
                'first_name': data['first_name'],
                'last_name': data['last_name'],
                'subjects': data['subjects_id'],
            },
            instance=teacher,
            serializer_class=TeacherSerializer
        )

        self.create_update(action, serializer)

    def teacher_delete(self, event):
        action = event['action']
        data = event['data']

        teacher = Teacher.objects.get(id=data['id'])
        self.delete(action, teacher)

    def teacher_list(self, event):
        action = event['action']

        queryset = Teacher.objects.all()
        serializer = self.new_serializer(
            serializer_class=TeacherSerializer,
            queryset=queryset,
            many=True
        )
        self.list(action, serializer)

    def teacher_details(self, event):
        action = 'details'
        data = event['data']

        teacher = Teacher.objects.get(id=data['id'])
        teacher_serializer = TeacherSerializer(teacher)

        self.send(
            text_data=json.dumps({
                'message': {
                    'action': action,
                    'data': {
                        'id': data['id'],
                        'teacher': teacher_serializer.data
                    }
                }
            })
        )
